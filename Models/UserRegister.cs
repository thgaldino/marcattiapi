using System;
using System.ComponentModel.DataAnnotations;

namespace MarcattiApi.Models
{
    public class UserRegister
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        public string Role { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        [EmailAddress(ErrorMessage = "O campo {0} esta em formato inválido.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        [StringLength(100, 
        ErrorMessage = "O campo {0} precisa ter entre {2} e {1} caracteres", 
        MinimumLength = 6)]
        public string Password { get; set; }

        //[Display(Name = "Nome Completo", Description = "Nome e Sobrenome.")]
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "O nome completo é obrigatório.")]
        [StringLength(100, 
        ErrorMessage = "O campo {0} precisa ter entre {2} e {1} caracteres", 
        MinimumLength = 6)]

        public string Username { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        [DataType(DataType.PhoneNumber)]
        [DisplayFormat(DataFormatString = "{0:##-####-####}")]
        public string Telephone { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        [DisplayFormat(DataFormatString = "{0:##-###-###-#}", 
        ApplyFormatInEditMode = true)]
        public string RG { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        [DataType(DataType.PhoneNumber)]
        [DisplayFormat(DataFormatString = "{0:##-#####-####}")]
        public string Mobile { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        [DisplayFormat(DataFormatString = "{0:###-###-###-##}", 
        ApplyFormatInEditMode = true)]
        public string CPF { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        [DataType(DataType.PhoneNumber)]
        [DisplayFormat(DataFormatString = "{0:##-#####-####}")]
        public string EmergPhone { get; set; }

        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }

        [DataType(DataType.Date)]
        public DateTime UpdatedAt { get; set; }
        
        [DataType(DataType.Date)]
        public DateTime SessionAt { get; set; }
    }
}