using System;
using System.ComponentModel.DataAnnotations;

namespace MarcattiApi.Models
{
    public class UserLogin
    {
        [Required(ErrorMessage = "Este campo é obrigatório")]
        [EmailAddress(ErrorMessage = "O campo {0} esta em formato inválido.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        [StringLength(100, 
        ErrorMessage = "O campo {0} precisa ter entre {2} e {1} caracteres", 
        MinimumLength = 6)]
        public string Password { get; set; }

        [DataType(DataType.Date)]
        public DateTime SessionAt { get; set; }
    }
}