using System;
using System.ComponentModel.DataAnnotations;

namespace MarcattiApi.Models
{
public class CustomerRegister
{
    [Key]
    public int Id { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    public string CustomerProfile { get; set; }

    [DataType(DataType.Date)]
    public DateTime UpdateDate { get; set; }
    
    [Required(ErrorMessage = "Digite o Nome Fantasia.")]
    [StringLength(100,
    ErrorMessage = "O nome fantasia deve ter até 100 caracteres",
    MinimumLength = 6)]
	public string FakeName { get; set; }

    [Required(ErrorMessage = "O nome completo é obrigatório.")]
    [StringLength(100,
    ErrorMessage = "O nome fantasia2 deve ter até 100 caracteres",
    MinimumLength = 6)]
    public string FakeName2 { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
	public string CNPJ { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    public string RegisterState { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    public bool Isento { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
	public string Address { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    [StringLength(10,
    ErrorMessage = "Preencha o Número do endereço.")]
	public string AddressNumber { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
	public string AddressAdjunct { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    public String Bairro { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    public String CEP { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    [RegularExpression("(AC|AL|AP|AM|BA|CE|DF|ES|GO|MA|MT|MS|MG|PA|PB|PR|PE|PI|RJ|RN|RS|RO|RR|SC|SP|SE|TO)", 
    ErrorMessage = "Formato inválido")]
	public string UF { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
	public string City { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    [StringLength(100,
    ErrorMessage = "O nome fantasia deve ter até 100 caracteres",
    MinimumLength = 6)]
    public string Name { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    [EmailAddress(ErrorMessage = "O campo {0} esta em formato inválido.")]
    public string Email { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    public string PhoneNumber { get; set; }
}
}
