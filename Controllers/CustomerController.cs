using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
// Core components
using MarcattiApi.Data;
using MarcattiApi.Models;

namespace MarcattiApi.Controllers
{
    [Route("v1/customer")]
    public class CustomerController : Controller
    {
        [HttpGet]
        [Route("")]
        [Authorize(Roles = "manager")]
        public async Task<ActionResult<List<CustomerRegister>>> 
        Get([FromServices] DataContext context)
        {
            var customers = await context
                .Customers
                .AsNoTracking()
                .ToListAsync();
            return customers;
        }

        [HttpPut]
        [Route("{id:int}")]
        [Authorize(Roles = "manager")]
        public async Task<ActionResult<CustomerRegister>> Put(
            [FromServices] DataContext context,
            int id,
            [FromBody]CustomerRegister model)
        {
            // Verifica se os dados são válidos
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            // Verifica se o ID informado é o mesmo do modelo
            if (id != model.Id)
                return NotFound(new { message = "Cliente não encontrada" });
            try
            {
                context.Entry(model).State = EntityState.Modified;
                await context.SaveChangesAsync();
                return model;
            }
            catch (Exception)
            {
                return BadRequest(new { message = "Não foi possível criar o cliente" });
            }
        }
    }
}