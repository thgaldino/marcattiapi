using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
// Core Components
using MarcattiApi.Data;
using MarcattiApi.Models;
using MarcattiApi.Services;

namespace MarcattiApi.Controllers
{
    [Route("v1/customers")]
    public class RegisterCustomerController : Controller
    {
        [HttpPost]
        [Route("register")]
        [AllowAnonymous]
        public async Task<ActionResult<CustomerRegister>> Post(
            [FromServices] DataContext context,
            [FromBody]CustomerRegister model)
        {
            // Verifica se os dados são válidos
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            try
            {
                context.Customers.Add(model);
                await context.SaveChangesAsync();

                return Ok("Cliente Cadastrado");
            }
            catch (Exception)
            {
                return BadRequest(new { message = "Não foi possível criar o cliente" });
            }
        }    
    }
}